from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
# See https://pycryptodome.readthedocs.io/en/latest/index.html

import math

def CRTinv(xp,xq,pinv,p,q,n):
	return (xp + (((xq-xp) * u) % q) * p) % n

if __name__=="__main__":

	# Read RSA keys
	# See https://pycryptodome.readthedocs.io/en/latest/src/public_key/rsa.html
	with open("3a-rsa-pub.pem",'rb') as key:
		pubkey = RSA.importKey(key.read())
	n = pubkey.n
	e = pubkey.e
	print(f"n: {n}")
	print(f"e: {e}")

	with open("3a-rsa-priv.key",'rb') as key:
		privkey = RSA.importKey(key.read())
	n = privkey.n
	e = privkey.e
	d = privkey.d
	p = privkey.p
	q = privkey.q
	# u = p^{-1} mod q
	u = privkey.u 
	print(f"n: {n}")
	print(f"e: {e}")
	print(f"d: {d}")
	print(f"p: {p}")
	print(f"q: {q}")
	print(f"u: {u}")
	
	# M = get_random_bytes(1024//8) 
	# print(M.hex())
	M = "37fbb188290f82179c2e1dc836fec375f8ddd3c5739dcd959c4002b10c55c4fa7d1cce2e72739111416025586c1448fcdaf43d29fa7402559bc241ec5e6e2d64b210c82e7a6e726216f044ce8d948f5250edc238f2d176a340149a3618bbb23ecd6ca2eb25c7bc6e0fd6e75e1311b426e4a098c2978d611b1bd05653dbe29189"
	print(M)

	sigma2 = pow(int(M,16),d,n)
	sigmap = pow(int(M,16),d,p)
	sigmaq = pow(int(M,16),d,q)
	sigma3 = CRTinv(sigmap,sigmaq,u,p,q,n)
	
	# sigma1 = faulty-pow(int(M,16),d,n)

	print(f"sigma3?= sigma2: {sigma3==sigma2}")
	
	# with open("3a-rsa-sig1.cert",'w') as cert1:
	#	cert1.write(f'{sigma1:#x}')
	#	
	# with open("3a-rsa-sig2.cert",'w') as cert2:
	#	cert2.write(f'{sigma2:#x}')


